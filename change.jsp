<!DOCTYPE html>
<%@ page import ="java.sql.*"%>
<%
if (session.getAttribute("did") == null)
{
	response.sendRedirect("index.jsp");
}

%>

<!--[if IE 8 ]><html class="no-js oldie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="no-js oldie ie9" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html class="no-js" lang="en"> <!--<![endif]-->

<head>

   <!--- basic page needs
   ================================================== -->
   <meta charset="utf-8">
	<title>Blood Bank</title>
	<meta name="description" content="">  
	<meta name="author" content="">

   <!-- mobile specific metas
   ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

 	<!-- CSS
   ================================================== -->
   <link rel="stylesheet" href="css/base.css">
   <link rel="stylesheet" href="css/vendor.css">  
   <link rel="stylesheet" href="css/main.css"> 
   <link rel="stylesheet" href="css/icons.css"> 

   <style type="text/css" media="screen">
   	#styles { 
   		background: white;
   		padding-top: 12rem;
   		padding-bottom: 12rem;
   	} 
		::placeholder{
		color : black;
		}
		
		.sub:disabled{
		border-color:blue;
		}



   </style>   

   <!-- script
   ================================================== -->
	<script src="js/modernizr.js"></script>
	<script src="js/pace.min.js"></script>

    <!--favicons
	================================================== -->
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
	<link rel="icon" href="favicon.ico" type="image/x-icon">

</head>

<body id="top" scroll="no" style="overflow: hidden">


   <!-- home
   ================================================== -->
   <section id="home-login">

   	<div class="overlay"></div>

   	<div class="home-content-table">	
		   <div class="home-content-tablecell">
		   	<div class="row" style="padding-top:40px;">
		   		<div class="col-twelve">	
						
			  				<h3 class="animate-intro">Change Password</h3><br/>					
				  			<center>
							
							<form action="fchange.jsp" method="POST" autocomplete="off">
								<input type="password" name="p" placeholder="Password" id="p">
								<input type="password"  placeholder="Confirm Password" id="cp" onblur="passmatch()">
								<p id="error"></p>
								<button type="submit" id="sub" disabled>Submit</button>
							</form>
							</center>
												
				</div> <!-- end col-twelve --> 
		   	</div> <!-- end row --> 
		   </div> <!-- end home-content-tablecell --> 		   
		</div> <!-- end home-content-table -->

		
   </section> <!-- end home -->
   <script src="js/jquery-2.1.3.min.js"></script>
   <script src="js/plugins.js"></script>
   <script src="js/main.js"></script>
	<script>
function passmatch()
{
	var p= document.getElementById("p").value;
	var cp= document.getElementById("cp").value;
	if(p!=cp)
	{
	 document.getElementById("error").innerHTML="Passwords dont match";
	 $("#sub").prop('disabled',true);
	}
	else
	{
	 document.getElementById("error").innerHTML=" ";	
	  $("#sub").prop('disabled',false);
	}
	
}
</script>
</body>

</html>