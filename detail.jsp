<!DOCTYPE html>
<%@ page import ="java.sql.*"%>
<%
if (session.getAttribute("did") == null)
{
	response.sendRedirect("index.jsp");
}

%>

<!--[if IE 8 ]><html class="no-js oldie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="no-js oldie ie9" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html class="no-js" lang="en"> <!--<![endif]-->

<head>

   <!--- basic page needs
   ================================================== -->
   <meta charset="utf-8">
	<title>Blood Bank</title>
	<meta name="description" content="">  
	<meta name="author" content="">

   <!-- mobile specific metas
   ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

 	<!-- CSS
   ================================================== -->
   <link rel="stylesheet" href="css/base.css">
   <link rel="stylesheet" href="css/vendor.css">  
   <link rel="stylesheet" href="css/main.css"> 
   <link rel="stylesheet" href="css/icons.css"> 

   <style type="text/css" media="screen">
   	#styles { 
   		background: white;
   		padding-top: 12rem;
   		padding-bottom: 12rem;
   	} 
		::placeholder{
		color : black;
		}
		
		.sub:disabled{
		border-color:blue;
		}



   </style>   

   <!-- script
   ================================================== -->
	<script src="js/modernizr.js"></script>
	<script src="js/pace.min.js"></script>

    <!--favicons
	================================================== -->
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
	<link rel="icon" href="favicon.ico" type="image/x-icon">

</head>

<body id="top" scroll="no" style="overflow: hidden">
<header>
<!--menu button-->   
		<a id="header-menu-trigger" href="#0">
		 	<span class="header-menu-text">Menu</span>
		  	<span class="header-menu-icon"></span>
		</a> 
<!--nav bar close-->
		<nav id="menu-nav-wrap">

			<a href="#0" class="close-button" title="close"><span>Close</span></a>	

	   	<h3>Detail</h3>  

			<ul class="nav-list">
				<li><a href="index.jsp">Home</a></li>
				<li><a href="search.jsp">Search</a></li>
				<li><a href="chpa.jsp">Change Password</a></li>
				<li><a href="logout.jsp">Logout</a></li>
			</ul>
</nav>  
</header>
<%

	
	Class.forName("com.mysql.jdbc.Driver");
    Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/bloodbank", "root", "");
	Statement st = con.createStatement();
    ResultSet rs;
	
	String s=(String)session.getAttribute("did");
	String q="select * from donor a left join donorb b on a.regno=b.dreg left join btype c on b.bno=c.bcode where a.regno='" + s + "' and b.dreg='"+s+"'";
	rs = st.executeQuery(q);
	
	if (rs.next()) {
		
%>
   <!-- home
   ================================================== -->
   <section id="home-login">

   	<div class="overlay"></div>

   	<div class="home-content-table">	
		   <div class="home-content-tablecell">
		   	<div class="row" style="padding-top:40px;">
		   		<div class="col-twelve">	
						
			  				<h3 class="animate-intro">Details</h3><br/>					
				  			<center>
							
							<form action="update.jsp" method="POST">
								<div class="col-six">
									Name:<input type="text" class="edit" disabled name="name" placeholder="Enter Your Name" value="<%= rs.getString("a.name")%>">
									Phone No:<input type="text" class="edit" disabled name="phno" placeholder="Enter The Phone no" value="<%= rs.getString("a.phno")%>">
									Age:<input type="number" class="edit" disabled name="age" value="<%= rs.getString("a.age")%>">
									Height:<input type="text" class="edit" disabled name="he" placeholder="Height" value="<%= rs.getString("a.height")%>">
								</div>	
								<div class="col-six">
									Weight:<input type="text" class="edit" disabled name="we" placeholder="Weight" value="<%= rs.getString("a.weight")%>">
									Address:<textarea name="add" class="edit" disabled placeholder="Enter Your Address" rows="2"><%= rs.getString("a.address")%></textarea>
									Email:<input type="email" disabled name="e" value="<%= rs.getString("a.eid")%>">
									Blood type:<input type="text" disabled name="btype" value="<%= rs.getString("c.bname")%>">
								</div>
								
								<input type="submit" value="Update" id="s" style="display:none;">
							</form>
				  			</center>
					<div>	
						<button onclick="unlock()" id="b">Unlock</button>			
					</div>							
				</div> <!-- end col-twelve --> 
		   	</div> <!-- end row --> 
		   </div> <!-- end home-content-tablecell --> 		   
		</div> <!-- end home-content-table -->

		
   </section> <!-- end home -->

<%
        
    } else {
	       response.sendRedirect("btype.jsp");
    }
	
 %>
  



  <script>
function unlock()
{
	
	
    $(".edit").prop('disabled',false);
    document.getElementById('b').style.display = "none";
	document.getElementById('s').style.display = "block";
}
</script>

   <script src="js/jquery-2.1.3.min.js"></script>
   <script src="js/plugins.js"></script>
   <script src="js/main.js"></script>
	
</body>

</html>