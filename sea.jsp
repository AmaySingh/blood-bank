		<%@ page import ="java.sql.*"%>
<%
if (session.getAttribute("did") == null)
{
	response.sendRedirect("index.jsp");
}

%>
<%
	
	int i=1;
	String s;
	if(session.getAttribute("s") == null)
	{	
	  s = request.getParameter("btype");
	 session.setAttribute("s",s);
	}
	else
	{
		 s=(String)session.getAttribute("s");
	}
	Class.forName("com.mysql.jdbc.Driver");
    Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/bloodbank", "root", "");
    Statement st = con.createStatement();
%>
		<!DOCTYPE html>
		<html>

		<head>

		   <!--- basic page needs
		   ================================================== -->
		   <meta charset="utf-8">
	<title>Blood Bank</title>
			<meta name="description" content="">  
			<meta name="author" content="">

		   <!-- mobile specific metas
		   ================================================== -->
			<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

			<!-- CSS
		   ================================================== -->
		   <link rel="stylesheet" href="css/base.css">
		   <link rel="stylesheet" href="css/vendor.css">  
		   <link rel="stylesheet" href="css/main.css"> 

		   <style type="text/css" media="screen">
			#styles { 
				background: white;
				padding-top: 12rem;
				padding-bottom: 12rem;
			}      	
			input:disabled{
			}
			#employee{
			border: 1px solid #cccccc;
			border-radius: 2px;
			font-size: 20px;
			color: black;
			padding: 5px;
			height: 250px;
			width: 100%;
		}
		#employee:hover{
			box-shadow: 0 0 2px 1px rgba(0, 140, 186, 0.5);
			background-color:black;
			}
			h4{
				color:white;
			}
		   </style>   

		   <!-- script
		   ================================================== -->
			<script src="js/modernizr.js"></script>
			<script src="js/pace.min.js"></script>
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		   <!-- favicons
			================================================== -->
			<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
			<link rel="icon" href="favicon.ico" type="image/x-icon">

		</head>

		<body id="top">
			 <!-- end #menu-nav-wrap -->
				
			<section  style="padding-top:0%; background-color:dark grey;">

			<h2 class="text-size-40 text-center text-m-size-30" style="padding-top: 1%"><b>EMPLOYEE DETAILS</b><br/><br/></h2>
			<div style="padding-bottom: 80px; margin-left: 5%; margin-right: 5%;">
			<%
String q="select * from donor a left join donorb b on a.regno=b.dreg left join btype c on b.bno=c.bcode where b.bno='"+s+"'";
ResultSet rs = st.executeQuery(q); 

while(rs.next()){
%>		
						<a href="disp.jsp?hid=<%= rs.getString("a.regno") %>">
						<div id="employee" style=" margin-bottom: 2%">
							<div class="row">
							
								<div class="col-twelve text-left" style="float: left; color: white;">
									<table class="text-size-20">
										<tr>
											<td class="text-left" >Index</td>
											<td class="text-left" ><% out.println(i++); %></td>
										</tr>
										<tr>
											<td class="text-left">Name</td>
											<td class="text-left"><%= rs.getString("a.name") %></td>
										</tr>
										<tr>
											<td class="text-left" >Age</td>
											<td class="text-left" ><%= rs.getString("a.age") %></td>
										</tr>
										<tr>
											<td class="text-left" >Contact no</td>
											<td class="text-left" ><%= rs.getString("a.phno") %></td>
										</tr>
										
										
									</table>
								</div>
							<!--<div class="col-sm-6" style="float:right">
								<img id="employee-pic" src="Employee-photo.jpg" alt="Employee">
							</div>-->
							</div>
						</div>
						</a>
<%
}
%>
			
			
					<!--<center><button type="button" onclick="window.location.href='search.php'">Back</button></center>-->
				
			
			
			<center>
					<div class="more animate-intro" style="text_align:center;">
										<a class="button stroke" href="search.jsp">
											Back
										</a>
									</div>		
				</center>
				</div>
				</section>
			
			<!--<form style="text-align: center">
			<input name="back" type="button" onclick="window.location.href='search.php'" class="ContentButtons" id="back" value="Go Back"></button>
			</form>-->
			

		   <!-- styles
		   ================================================== -->
			

		   <div id="preloader"> 
				<div id="loader"></div>
		   </div> 
		
		   <!-- Java Script
		   ================================================== --> 
		   <script src="js/jquery-2.1.3.min.js"></script>
		   <script src="js/plugins.js"></script>
		   <script src="js/main.js"></script>
		</section>
		</body>
		</html>
