<%@ page import ="java.sql.*"%>
<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8">
	<title>Blood Bank</title>
	<link rel="stylesheet" href="css/base.css">
    <link rel="stylesheet" href="css/vendor.css">  
    <link rel="stylesheet" href="css/main.css">  
	<script src="js/modernizr.js"></script>
	<script src="js/pace.min.js"></script>
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
	<link rel="icon" href="favicon.ico" type="image/x-icon">
</head>

<body id="tophome">

	<!-- header 
   ================================================== -->
   <header>
<!--menu button-->   
		<a id="header-menu-trigger" href="#0">
		 	<span class="header-menu-text">Menu</span>
		  	<span class="header-menu-icon"></span>
		</a> 
<!--nav bar close-->
		<nav id="menu-nav-wrap">

			<a href="#0" class="close-button" title="close"><span>Close</span></a>	

	   	<h3>Blood Bank</h3>  

			<ul class="nav-list">
				<li class="current"><a class="smoothscroll" href="#home" title="">Home</a></li>
				<li><a class="smoothscroll" href="#about" title="">About</a></li>
				<li><a class="smoothscroll" href="#services" title="">Services</a></li>			
		<%
if (session.getAttribute("did") == null)
{
%>
<li><a href="login.html">Login</a></li>
<li><a href="reg.html">Register</a></li>
<%
}
else
{
%>
	<li><a href="detail.jsp">Detail</a></li>
	<li><a href="search.jsp">Search</a></li>
	<li><a href="chpa.jsp">Change Password</a></li>
	<li><a href="logout.jsp">Logout</a></li>
<%
}
%>
</ul>
</nav>  
</header><section id="home">

   	<div class="overlay"></div>

   	<div class="home-content-table">	
		   <div class="home-content-tablecell">
		   	<div class="row">
		   		<div class="col-twelve">		   			
			  		
			  				<h3 class="animate-intro">Blood Bank</h3>
				  			<h1 class="animate-intro">
							"Donate your blood for a reason, let the reason to be life"
				  			</h1>	

				  			<div class="more animate-intro">
				  				<a class="smoothscroll button stroke" href="#about">
				  					Learn More
				  				</a>
				  			</div>							

			  		</div> <!-- end col-twelve --> 
		   	</div> <!-- end row --> 
		   </div> <!-- end home-content-tablecell --> 		   
		</div> <!-- end home-content-table -->

		
		<div class="scrolldown">
			<a href="#about" class="scroll-icon smoothscroll">		
		   	Scroll Down		   	
		   	<i class="fa fa-long-arrow-right" aria-hidden="true"></i>
			</a>
		</div>			
   
   </section> <!-- end home -->


   <!-- about
   ================================================== -->
   <section id="about">

   	<div class="row about-wrap">
   		<div class="col-full">

   			<div class="about-profile-bg"></div>

				<div class="intro">
					<h3 class="animate-this">About The Project</h3>
	   				<p style="font-size:20px;">
					The aim of this project was to create a database of all viable donors on 
					hand so that searching for blood does not become more complicated than saving a life. 
					This blood bank aims at providing a fast and a more efficent means of tacking down intersted donors 
					in and around them. The project has been made for the people of Anna University and can help in
					times of utmost need.
					</p>
				</div>   

   		</div> <!-- end col-full  -->
   	</div> <!-- end about-wrap  -->

   </section> <!-- end about -->


   <!-- about
   ================================================== -->
   <section id="services">
   	<div class="row narrow section-intro with-bottom-sep animate-this">
   		<div class="col-full">
   			
   				<h3>Project By</h3>
   			   <h1>About us</h1>
   			
   			   <p class="lead">The project was created by Amay Singh and Aravind Prakash. We are two 2nd year students who took on this
			   course to learn about JSP, HTML, MYsql, CSS, JS, JQuery and other functionalities.</p>
   			
   	   </div> <!-- end col-full -->
   	</div> <!-- end row -->
      </section>
   <div id="preloader"> 
    	<div id="loader"></div>
   </div> 
   <script src="js/jquery-2.1.3.min.js"></script>
   <script src="js/plugins.js"></script>
   <script src="js/main.js"></script>

</body>

</html>
