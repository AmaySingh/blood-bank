<!DOCTYPE html>
<%@ page import ="java.sql.*"%>
<%
if (session.getAttribute("did") == null)
{
	response.sendRedirect("index.jsp");
}
session.setAttribute("s",null);
%>

<!--[if IE 8 ]><html class="no-js oldie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="no-js oldie ie9" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html class="no-js" lang="en"> <!--<![endif]-->

<head>

   <!--- basic page needs
   ================================================== -->
   <meta charset="utf-8">
	<title>Blood Bank</title>
	<meta name="description" content="">  
	<meta name="author" content="">

   <!-- mobile specific metas
   ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

 	<!-- CSS
   ================================================== -->
   <link rel="stylesheet" href="css/base.css">
   <link rel="stylesheet" href="css/vendor.css">  
   <link rel="stylesheet" href="css/main.css"> 
   <link rel="stylesheet" href="css/icons.css"> 

   <style type="text/css" media="screen">
   	#styles { 
   		background: white;
   		padding-top: 12rem;
   		padding-bottom: 12rem;
   	} 
		::placeholder{
		color : black;
		}
		
		.sub:disabled{
		border-color:blue;
		}



   </style>   

   <!-- script
   ================================================== -->
	<script src="js/modernizr.js"></script>
	<script src="js/pace.min.js"></script>

    <!--favicons
	================================================== -->
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
	<link rel="icon" href="favicon.ico" type="image/x-icon">

</head>

<body id="top" scroll="no" style="overflow: hidden">
<header>
<!--menu button-->   
		<a id="header-menu-trigger" href="#0">
		 	<span class="header-menu-text">Menu</span>
		  	<span class="header-menu-icon"></span>
		</a> 
<!--nav bar close-->
		<nav id="menu-nav-wrap">

			<a href="#0" class="close-button" title="close"><span>Close</span></a>	

	   	<h3>Search</h3>  

			<ul class="nav-list">
				<li><a href="index.jsp">Home</a></li>
				<li><a href="detail.jsp">Detail</a></li>
				<li><a href="chpa.jsp">Change Password</a></li>
				<li><a href="logout.jsp">Logout</a></li>
			</ul>
</nav>  
</header>

   <!-- home
   ================================================== -->
   <section id="home-login">

   	<div class="overlay"></div>

   	<div class="home-content-table">	
		   <div class="home-content-tablecell">
		   	<div class="row" style="padding-top:40px;">
		   		<div class="col-twelve">	
						
			  				<h3 class="animate-intro">Search</h3><br/>					
				  			<center>
							
							<form action="sea.jsp" method="POST" autocomplete="off">
								<select name="btype" onblur="opti()" class='formboxes_sel' id="opt" style="width:20%">
									<option value=" ">------</option>
									<option value="1">O+ve</option>
									<option value="2">O-ve</option>
									<option value="3">A+ve</option>
									<option value="4">B+ve</option>
									<option value="7">A-ve</option>
									<option value="8">B-ve</option>
									<option value="5">AB+ve</option>
									<option value="6">AB-ve</option>
								</select>
								<button type="submit">Search</button>
							</form>
				  			</center>
												
				</div> <!-- end col-twelve --> 
		   	</div> <!-- end row --> 
		   </div> <!-- end home-content-tablecell --> 		   
		</div> <!-- end home-content-table -->

		
   </section> <!-- end home -->
   <script src="js/jquery-2.1.3.min.js"></script>
   <script src="js/plugins.js"></script>
   <script src="js/main.js"></script>
	
</body>

</html>